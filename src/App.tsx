import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonMenu,
  IonMenuToggle,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  IonTitle,
  IonToolbar,
  setupIonicReact
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { bagOutline, cellularOutline, ellipse, gridOutline, heartOutline, menuOutline, personAddOutline, personCircleOutline, searchOutline, square, triangle } from 'ionicons/icons';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import Search from './pages/Tab2';
import Inicio from './pages/Tab1';
import Heart from './pages/Tab3';
import Bag from './pages/Tab4';

setupIonicReact();

const App: React.FC = () => (
  <IonApp>

    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route exact path="/inicio">
            <Inicio />
          </Route>
          <Route exact path="/search">
            <Search />
          </Route>
          <Route path="/heart">
            <Heart />
          </Route>
          <Route path="/bag">
            <Bag />
          </Route>
          <Route exact path="/">
            <Redirect to="/inicio" />
          </Route>
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton tab="tab1" href="/inicio">
            <IonIcon icon={gridOutline} />
          </IonTabButton>
          <IonTabButton tab="tab2" href="/search">
            <IonIcon icon={searchOutline} />
          </IonTabButton>
          <IonTabButton tab="tab3" href="/heart">
            <IonIcon icon={heartOutline} />
          </IonTabButton>
          <IonTabButton tab="tab4" href="/bag">
            <IonIcon icon={bagOutline} />
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
