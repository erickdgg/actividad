import { IonAvatar, IonButton, IonCard, IonCardContent, IonCardSubtitle, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonItemOptions, IonItemSliding, IonList, IonPage, IonRow, IonTitle, IonToolbar } from '@ionic/react';
import { chevronBackOutline, ellipsisHorizontalOutline, logoUsd, trashOutline } from 'ionicons/icons';
import './Tab4.css';
import tenis1 from '../img/tenis1.jpg';
import tenis2 from '../img/tenis2.jpg';
import tenis3 from '../img/tenis3.jpg';
import tenis4 from '../img/tenis4.jpg';
import tenis5 from '../img/tenis2.jpg';
import tenis6 from '../img/tenis6.jpg';
import tenis7 from '../img/tenis7.jpg';


const Bag: React.FC = () => {
  return (
    <IonPage>
      <IonContent>
      <IonGrid>

        <IonRow>
          <IonIcon id='icono' size='large' icon={chevronBackOutline}></IonIcon>

          <IonCard id='card3'>
          <IonIcon id="icono2" size='large' icon={ellipsisHorizontalOutline}></IonIcon>
          </IonCard>

        </IonRow>
        <IonCol>
        <IonCardTitle id='tisu'>Your Cart</IonCardTitle>
        <IonCardSubtitle id='tisu'>Check and Payment</IonCardSubtitle>

        <IonList>
        <IonItemSliding>
          <IonRow>
          <IonCard>
            <IonCardContent>
            <img src={tenis1}/>
            </IonCardContent>
          </IonCard>
          </IonRow>
          <IonItemOptions side='end'>
            <button color='danger'>
              <IonIcon icon={trashOutline}> </IonIcon>
              Eliminar
            </button>
          </IonItemOptions>

        </IonItemSliding>


        <IonItemSliding>
          <IonRow>
          <IonCard>
            <IonCardContent>
            <img src={tenis1}/>
            </IonCardContent>
          </IonCard>
          </IonRow>
          <IonItemOptions side='end'>
            <button color='danger'>
              <IonIcon icon={trashOutline}> </IonIcon>
              Eliminar
            </button>
          </IonItemOptions>

        </IonItemSliding>


        <IonItemSliding>
          <IonRow>
          <IonCard>
            <IonCardContent>
            <img src={tenis1}/>
            </IonCardContent>
          </IonCard>
          </IonRow>
          <IonItemOptions side='end'>
            <button color='danger'>
              <IonIcon icon={trashOutline}> </IonIcon>
              Eliminar
            </button>
          </IonItemOptions>

        </IonItemSliding>
        
        </IonList>

        <IonRow>
          <IonCardSubtitle>3 Items</IonCardSubtitle>  <IonIcon color="warning" icon={logoUsd}></IonIcon> <IonCardTitle>459,22</IonCardTitle>
        </IonRow>

        <IonButton color="warning" size='large'>
          Check Out
        </IonButton>

        </IonCol>

        </IonGrid>
      
      </IonContent>
    </IonPage>
  );
};

export default Bag;

{/* <IonItemSliding>
          <IonRow>
          <IonCard>
            <IonCardContent>
            <img src={tenis1}/>
            </IonCardContent>
            <IonCol>
              <IonCardTitle>Nike Joyride Run Fly...</IonCardTitle>
              <IonCardSubtitle>Women's Shoe</IonCardSubtitle>
              <IonRow><IonIcon color="warning" icon={logoUsd}></IonIcon> <IonCardTitle>183.02</IonCardTitle></IonRow>
            </IonCol>
          </IonCard>
          </IonRow>
          <IonItemOptions side='end'>
            <button color='danger'>
              <IonIcon icon={trashOutline}> </IonIcon>
              Eliminar
            </button>
          </IonItemOptions>

        </IonItemSliding>


        <IonItemSliding>
          <IonRow>
          <IonCard>
            <IonCardContent>
            <img src={tenis1}/>
            </IonCardContent>
            <IonCol>
              <IonCardTitle>Nike Joyride Run Fly...</IonCardTitle>
              <IonCardSubtitle>Women's Shoe</IonCardSubtitle>
              <IonRow><IonIcon color="warning" icon={logoUsd}></IonIcon> <IonCardTitle>183.02</IonCardTitle></IonRow>
            </IonCol>
          </IonCard>
          </IonRow>
          <IonItemOptions side='end'>
            <button color='danger'>
              <IonIcon icon={trashOutline}> </IonIcon>
              Eliminar
            </button>
          </IonItemOptions>

        </IonItemSliding>


        <IonItemSliding>
          <IonRow>
          <IonCard>
            <IonCardContent>
            <img src={tenis1}/>
            </IonCardContent>
            <IonCol>
              <IonCardTitle>Nike Joyride Run Fly...</IonCardTitle>
              <IonCardSubtitle>Women's Shoe</IonCardSubtitle>
              <IonRow><IonIcon color="warning" icon={logoUsd}></IonIcon> <IonCardTitle>183.02</IonCardTitle></IonRow>
            </IonCol>
          </IonCard>
          </IonRow>
          <IonItemOptions side='end'>
            <button color='danger'>
              <IonIcon icon={trashOutline}> </IonIcon>
              Eliminar
            </button>
          </IonItemOptions>

        </IonItemSliding> */}
