import { IonApp, IonAvatar, IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonChip, IonCol, IonContent, IonFab, IonFabButton, IonGrid, IonHeader, IonIcon, IonItem, IonLabel, IonPage, IonRow, IonSegment, IonSegmentButton, IonSlide, IonSlides, IonTitle, IonToolbar } from '@ionic/react';
import { chevronForwardOutline, heartCircle, heartCircleOutline, logoUsd, menuOutline } from 'ionicons/icons';
import './Tab1.css';
import tenis1 from '../img/tenis1.jpg';
import tenis2 from '../img/tenis2.jpg';
import tenis3 from '../img/tenis3.jpg';
import tenis4 from '../img/tenis4.jpg';
import tenis5 from '../img/tenis2.jpg';
import tenis6 from '../img/tenis6.jpg';
import tenis7 from '../img/tenis7.jpg';
import persona from '../img/persona.jpg';

const Inicio: React.FC = () => {

  const opcion = {
    slidesPerView: 1.9
  }

  const colors = {
    
  }

  return (
    <IonPage>
      
      <IonHeader></IonHeader>
      
        
        <IonContent fullscreen>
        <IonGrid>

          <IonRow>
            <IonIcon id='icono' size='large' icon={menuOutline}></IonIcon>

            <IonCard id="card">
          <IonAvatar>
            <img id="imagen" src={persona}/>
          </IonAvatar>
          </IonCard>

          <IonSegment color="warning">
            <IonSegmentButton color='warning' value='uno'>
              <IonLabel>Popular</IonLabel>
            </IonSegmentButton>
            <IonSegmentButton value='dos'>
              <IonLabel>New Release</IonLabel>
            </IonSegmentButton>
            <IonSegmentButton value='tres'>
              <IonLabel>Men</IonLabel>
            </IonSegmentButton>
            <IonSegmentButton value='cuatro'>
              <IonLabel>Women</IonLabel>
            </IonSegmentButton>
          </IonSegment>

          <IonSlides>
            
            <IonSlide>
              <IonCard>
              <IonCardContent>
                <img src={tenis1}/>
              </IonCardContent>
              <IonCardHeader>
                <IonCardTitle>Nike Joride run</IonCardTitle>
                <IonCardTitle>FlyKnit</IonCardTitle>
                <IonCardSubtitle>Women's Shoe</IonCardSubtitle>
                <IonRow>
                <IonIcon color="warning" icon={logoUsd}></IonIcon><IonCardTitle>183.02</IonCardTitle><IonIcon color="warning" size='large' icon={heartCircleOutline}></IonIcon>
                </IonRow>
              </IonCardHeader>
              </IonCard>
              </IonSlide>

              <IonSlide>
              <IonCard>
              <IonCardContent>
                <img src={tenis1}/>
              </IonCardContent>
              <IonCardHeader>
                <IonCardTitle>Nike Joride run</IonCardTitle>
                <IonCardTitle>FlyKnit</IonCardTitle>
                <IonCardSubtitle>Women's Shoe</IonCardSubtitle>
                <IonRow>
                <IonIcon color="warning" icon={logoUsd}></IonIcon><IonCardTitle>183.02</IonCardTitle><IonIcon color="warning" size='large' icon={heartCircleOutline}></IonIcon>
                </IonRow>
              </IonCardHeader>
              </IonCard>
              </IonSlide>

              <IonSlide>
              <IonCard>
              <IonCardContent>
                <img src={tenis1}/>
              </IonCardContent>
              <IonCardHeader>
                <IonCardTitle>Nike Joride run</IonCardTitle>
                <IonCardTitle>FlyKnit</IonCardTitle>
                <IonCardSubtitle>Women's Shoe</IonCardSubtitle>
                <IonRow>
                <IonIcon color="warning" icon={logoUsd}></IonIcon><IonCardTitle>183.02</IonCardTitle><IonIcon color="warning" size='large' icon={heartCircleOutline}></IonIcon>
                </IonRow>
              </IonCardHeader>
              </IonCard>
              </IonSlide>

              <IonSlide>
              <IonCard>
              <IonCardContent>
                <img src={tenis1}/>
              </IonCardContent>
              <IonCardHeader>
                <IonCardTitle>Nike Joride run</IonCardTitle>
                <IonCardTitle>FlyKnit</IonCardTitle>
                <IonCardSubtitle>Women's Shoe</IonCardSubtitle>
                <IonRow>
                <IonIcon color="warning" icon={logoUsd}></IonIcon><IonCardTitle>183.02</IonCardTitle><IonIcon color="warning" size='large' icon={heartCircleOutline}></IonIcon>
                </IonRow>
              </IonCardHeader>
              </IonCard>
              </IonSlide>

            </IonSlides>

          </IonRow>

          <IonCol>

          <IonCard>
              <IonRow>
              <IonCard id='cardd'>
              <IonCardContent>
                <img src={tenis1}/>
              </IonCardContent>
              </IonCard>
              <IonCardHeader>
                <IonCol>
                <IonCardTitle>Nike Air Max 270</IonCardTitle>
                <IonCardSubtitle>Men's Shoe</IonCardSubtitle>
                </IonCol> 
                <IonRow>
                  <IonIcon color="warning" icon={logoUsd}></IonIcon><IonCardTitle>162.69</IonCardTitle><IonIcon color="warning" size='large' icon={chevronForwardOutline}></IonIcon>
                </IonRow>
              </IonCardHeader>
              </IonRow>
              </IonCard>

              <IonCard>
              <IonRow>
              <IonCard id='cardd'>
              <IonCardContent>
                <img src={tenis1}/>
              </IonCardContent>
              </IonCard>
              <IonCardHeader>
                <IonCol>
                <IonCardTitle>Nike Air Max 270</IonCardTitle>
                <IonCardSubtitle>Men's Shoe</IonCardSubtitle>
                </IonCol> 
                <IonRow>
                  <IonIcon color="warning" icon={logoUsd}></IonIcon><IonCardTitle>162.69</IonCardTitle><IonIcon color="warning" size='large' icon={chevronForwardOutline}></IonIcon>
                </IonRow>
              </IonCardHeader>
              </IonRow>
              </IonCard>

              <IonCard>
              <IonRow>
              <IonCard id='cardd'>
              <IonCardContent>
                <img src={tenis1}/>
              </IonCardContent>
              </IonCard>
              <IonCardHeader>
                <IonCol>
                <IonCardTitle>Nike Air Max 270</IonCardTitle>
                <IonCardSubtitle>Men's Shoe</IonCardSubtitle>
                </IonCol> 
                <IonRow>
                  <IonIcon color="warning" icon={logoUsd}></IonIcon><IonCardTitle>162.69</IonCardTitle><IonIcon color="warning" size='large' icon={chevronForwardOutline}></IonIcon>
                </IonRow>
              </IonCardHeader>
              </IonRow>
              </IonCard>

              <IonCard>
              <IonRow>
              <IonCard id='cardd'>
              <IonCardContent>
                <img src={tenis1}/>
              </IonCardContent>
              </IonCard>
              <IonCardHeader>
                <IonCol>
                <IonCardTitle>Nike Air Max 270</IonCardTitle>
                <IonCardSubtitle>Men's Shoe</IonCardSubtitle>
                </IonCol> 
                <IonRow>
                  <IonIcon color="warning" icon={logoUsd}></IonIcon><IonCardTitle>162.69</IonCardTitle><IonIcon color="warning" size='large' icon={chevronForwardOutline}></IonIcon>
                </IonRow>
              </IonCardHeader>
              </IonRow>
              </IonCard>

          </IonCol>
          

          

        </IonGrid>

        </IonContent>
    </IonPage>
  );
};

export default Inicio;
