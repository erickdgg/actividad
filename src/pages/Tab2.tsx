import { IonAvatar, IonButton, IonCard, IonCardContent, IonCardSubtitle, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonLabel, IonPage, IonRow, IonTitle, IonToolbar } from '@ionic/react';
import { bagAddOutline, bagOutline, chevronBackOutline, heart, logoUsd } from 'ionicons/icons';
import './Tab2.css';
import tenis1 from '../img/tenis1.jpg';
import tenis2 from '../img/tenis2.jpg';
import tenis3 from '../img/tenis3.jpg';
import tenis4 from '../img/tenis4.jpg';
import tenis5 from '../img/tenis2.jpg';
import tenis6 from '../img/tenis6.jpg';
import tenis7 from '../img/tenis7.jpg';

const Search: React.FC = () => {
  return (
    <IonPage>
      <IonContent>
      <IonGrid>

        <IonRow>
          <IonIcon id='icono' size='large' icon={chevronBackOutline}></IonIcon>

          <IonCard id="card">
        <IonAvatar id="avatar">
          <IonIcon color="warning" size='large' icon={heart}></IonIcon>
        </IonAvatar>
        </IonCard>

        </IonRow>
        <IonCol>
          <IonCard>
            <IonCardContent>
              <img src={tenis1}/>
            </IonCardContent>
          </IonCard>

          <IonRow>
          <IonCard id="card2">
          <IonAvatar>
            <img id="imagen" src={tenis1}/>
          </IonAvatar>
          </IonCard>

          <IonCard id="card2">
          <IonAvatar>
            <img id="imagen" src={tenis1}/>
          </IonAvatar>
          </IonCard>

          <IonCard id="card2">
          <IonAvatar>
            <img id="imagen" src={tenis1}/>
          </IonAvatar>
          </IonCard>

          <IonCard id="card2">
          <IonAvatar>
            <img id="imagen" src={tenis1}/>
          </IonAvatar>
          </IonCard>

          </IonRow>

          <IonButton color="warning" size='large' id='bag'>
            <IonLabel>Buy</IonLabel>
            <IonIcon icon={bagOutline}></IonIcon>
          </IonButton>

          <IonCardSubtitle id='sub'>Women's Shoe</IonCardSubtitle>
          <IonCardTitle id='sub2'>Nike Joyride Run FlyKnit</IonCardTitle>
          <IonRow><IonIcon id='sub2' color="warning" icon={logoUsd}></IonIcon> <IonTitle id='sub2'>183.02</IonTitle></IonRow>
          <IonCardSubtitle id='sub'>The Nike Joyride Run FlyKnit is designed to help</IonCardSubtitle>
          <IonCardSubtitle id='sub'>meka runing feel easier and give your legs day.</IonCardSubtitle>
          <IonCardSubtitle id='sub'>Tiny foam beads underfoot contour to your foot.</IonCardSubtitle>
          <IonRow> <IonCardTitle id='sub2'>Best Selling</IonCardTitle> <IonCardSubtitle slot='end'>Size Guide</IonCardSubtitle> </IonRow>

          <IonRow >
          <IonButton>
            <IonLabel>US 5</IonLabel>
          </IonButton>  

          <IonButton>
            <IonLabel color="warning">US 6</IonLabel>
          </IonButton>  

          <IonButton>
            <IonLabel>US 7</IonLabel>
          </IonButton>  

          <IonButton>
            <IonLabel>US 8</IonLabel>
          </IonButton>  
            
            </IonRow>

            <IonRow>

            <IonIcon icon=''></IonIcon>  

            <IonCardSubtitle>View Product Detail</IonCardSubtitle>
            
            </IonRow>     
        </IonCol>



        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Search;
